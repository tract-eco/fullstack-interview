/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ProductView } from './ProductView';

export type ProductsView = {
    data: Array<ProductView>;
    count: number;
};

